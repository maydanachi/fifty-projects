import React, { useState } from 'react'
import './ExpandingCardsStyles.css'
import { pics } from './resources.js'

export default function ExpandingCards() {

    const NO_SELECTION = 'none'
    const [selected, setSelected] = useState(NO_SELECTION)

    function onClick(newIndex) {
        setSelected(prevIndex => {
            return newIndex === prevIndex
                ? NO_SELECTION
                : newIndex
        })
    }

    return <div className='container'>
        {
            pics.map((pic, index) => {
                const { src, label } = pic
                const hasSelection = selected !== NO_SELECTION
                const isSelected = selected === index
                const className = `panel ${hasSelection && isSelected ? 'active' : ''}`
                return <div
                    key={index}
                    className={className}
                    style={{ backgroundImage: `url(${src})` }}
                    onClick={() => onClick(index)}
                >
                    <div className='label'>
                        {label}
                    </div>
                </div>
            })
        }
    </div>
}