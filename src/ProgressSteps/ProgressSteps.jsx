import React, { useState } from 'react'
import { mockSteps } from './resources'
import './ProgressSteps.css'

export default function ProgressSteps() {
    
    const [steps, setActive] = useState({
            list: mockSteps,
            size: mockSteps.length,
            current: 0
        })

    function handleNavigation(increment) {
        setActive((steps) => {

            const newActive = steps.current + increment
            const isValid = newActive >= 0 && newActive < steps.size

            return {
                ...steps,
                current: isValid ? newActive : steps.current,
            }
        })
    }

    const { current, size } = steps
    const isFirst = current === 0
    const isLast = current === size - 1
    const progress = `${100*current/(size - 1)}%`

    return <div className='container'>
        <div className='progress-container'>
            <div className='progress' style={{ width: progress }} />
            {
                steps.list.map(({ id, value }, i) => {

                    const { current } = steps
                    const isActive = i <= current
                    const className = `circle ${ isActive ? 'active' : '' }`

                    return <div
                        key={id}
                        className={className}
                    >
                        {value}
                    </div>
                })
            }
        </div>
        <div className='actions'>
            <button
                id='prev'
                disabled={isFirst}
                onClick={() => handleNavigation(-1)
            }>
                Prev
            </button>
            <button
                id='next'
                disabled={isLast}
                onClick={() => handleNavigation(1)
            }>
                Next
            </button>
        </div>
    </div>
}